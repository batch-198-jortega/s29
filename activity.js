db.courses.insertMany([
    {
        name: "HTML Basics",
        price: 20000,
        isActive: true,
        instructor: "Sir Alvin"
    },
    {
        name: "CSS 101",
        price: 21000,
        isActive: true,
        instructor: "Sir Alvin"
    },    
    {
        name: "Javascript 101",
        price: 32000,
        isActive: true,
        instructor: "Ma'am Tine"
    },
    {
        name: "Git 101, IDE and CLI",
        price: 19000,
        isActive: false,
        instructor: "Ma'am Tine"
    },
    {
        name: "React.Js 101",
        price: 25000,
        isActive: true,
        instructor: "Ma'am Miah"
    }

])
 
//Find courses: instructor: Sir Alvin AND GTE 20000; show name and price only 
db.courses.find({$and:[{instructor:{$regex:"Sir Alvin",$options:'i'}},{price:{$gte:20000}}]},{_id:0,name:1,price:1})

//Find courses: instructor: Ma'am Tine AND INACTIVE; show name and price only 
db.courses.find({$and:[{instructor:{$regex:"Ma'am Tine",$options:'i'}},{isActive:false}]},{_id:0,name:1,price:1})

//Find courses: letter 'r' in NAME AND price LOWER or EQUAL to 25000;
db.courses.find({$and:[{name:{$regex:'r',$options:'i'}},{price:{$lte:25000}}]})

// UPDATE ALL courses w/ price LESS THAN($lt) 21000 to INACTIVE($set:)
db.courses.updateMany({price:{$lt:21000}},{$set:{isActive:false}})

// DELETE ALL courses w/ price GTE 25000
db.courses.deleteMany({price:{$gte:25000}})


    
    
    
    