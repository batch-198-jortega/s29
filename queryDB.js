// db.products.insertMany ([
// // in mongoDB, it is okay that they keys don't have quotation marks. 
// It will be converted into a string in Javascript
//     {
//         name: "Iphone X",
//         price: 30000,
//         isActive: true
//     },
//     {
//         name: "Samsung Galaxy S21",
//         price: 51000,
//         isActive: true
//     },
//     {
//         name: "Razer Blackshark V2X",
//         "price": 2800,
//         "isActive": false
//     },
//     {
//         name: "RAKK Gaming Mouse",
//         price: 1800,
//         isActive: true
//     },
//     {
//         name: "Razer Mechanical Keyboard",
//         price: 4000,
//         isActive: true
//     }
// ])

// Query Operators
// Allows us to expand our queries and define conditions 
// instead of just looking for specific values

// $gt,$lt,$gte,$lte

// $gt - query operator which means greater than
// db.products.find({price:{$gt: 3000}})

// $lt - query operator which means less than
// db.products.find({price:{$lt: 3000}})

// $gte - query operator which means greater than or equal to 
// db.products.find({price:{$gte: 30000}})

// $lte - query operator which means less than or equal to 
// db.products.find({price:{$lte: 2800}})

// db.newUsers.insertMany([
// 	{
// 	        firstName: "Mary Jane",
// 		lastName: "Watson",
// 		email: "mjtiger@gmail.com",
// 		password: "tigerjackpot15",
// 		isAdmin: false
// 	},
// 	{
// 		firstName: "Gwen",
// 		lastName: "Stacy",
// 		email: "stacyTech@gmail.com",
// 		password: "stacyTech1991",
// 		isAdmin: true
// 	},
// 	{
// 		firstName: "Peter",
// 		lastName: "Parker",
// 		email: "peterWebDev@gmail.com",
// 		password: "webdeveloperPeter",
// 		isAdmin: true
// 	},
// 	{
// 		firstName: "Jonah",
// 		lastName: "Jameson",
// 		email: "jjjameson@gmail.com",
// 		password: "spideyisamenace",
// 		isAdmin: false
// 
// 	},
// 	{
// 		firstName: "Otto",
// 		lastName: "Octavius",
// 		email: "ottoOctopi@gmail.com",
// 		password: "docOck15",
// 		isAdmin: true
// 	}
// ])

// $regex - query operator which will allow us to find documents which will match the
// character/pattern of the characters we are looking for.

// $regex looks for documents with partial match and by default is case sensitive
db.users.find({firstName:{$regex: 'O'}})

// $options: '$i' - used in our regex so that it will be case insensitive 
db.users.find({firstName:{$regex:'o',$options:'$i'}})

// You can also find for ducoments with partial matches
db.products.find({name:{$regex:'phone',$options:'$i'}})

// Find users whose email have the word "web" in it
db.users.find({email:{$regex:'web',$options:'i'}})

// Mini-Activity

//      1. using $regex look for products which name has the word "razer" in it.
//      2. using $regex look for products which name has the word "rakk" in it.

    db.products.find({name:{$regex:'razer',$options:'i'}})
    db.products.find({name:{$regex:'rakk',$options:'i'}})

// $or $and - logical operators - works almost the same way as they in JS 
// $or - allos us to have a logical operation to find for documents which can satisfy
// at least 1 of our conditions

db.products.find({$or:[{name:{$regex:'x',$options:'$i'}},{price:{$lte:10000}}]})
db.products.find({$or:[{name:{$regex:'x',$options:'i'}},{price:{$gte:30000}}]})

// $and - look or find for documents that satisfies both/all conditions

db.products.find({$and:[{name:{$regex:'razer',$options:'i'}},{price:{$gte:3000}}]})
db.products.find({$and:[{name:{$regex:'x',$options:'i'}},{price:{$gte:30000}}]})
db.users.find({$and:[{lastName:{$regex:'w',$options:'i'}},{isAdmin:false}]})
db.users.find({$and:[{firstName:{$regex:'a',$options:'i'}},{isAdmin:true}]})

// Field Projection - allow us to show/hide certain properties/fields
// db.collection.find({},{})

db.users.find({},{_id:0,password:0})

db.users.find({isAdmin:true},{_id:0,email:1})
// _id field must be explicitly hidden if you don't want it to show in the results "_id:0".
// We can also just pick which fields to show.
db.users.find({isAdmin:false},{firstName:1,lastName:1})

db.products.find({price:{$gte:10000}},{_id:0,name:1,price:1})
